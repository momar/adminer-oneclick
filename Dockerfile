FROM adminer:standalone

ENV ADMINER_DESIGN=pepa-linha

USER 0
ADD --chown=adminer:adminer https://raw.githubusercontent.com/giofreitas/one-click-login/master/oneclick-login.php plugins/
RUN sed -i 's/echo \$this->driver/echo $server["driver"]/g' plugins/oneclick-login.php
COPY oneclick-login.php plugins-enabled/00-oneclick-login.php
USER adminer
