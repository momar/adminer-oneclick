image:
	docker build --compress -t momar/adminer-oneclick .

deploy: image
	docker push momar/adminer-oneclick
