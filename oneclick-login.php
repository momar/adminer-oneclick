<?php
require_once('./plugins/oneclick-login.php');

function env(...$fallbacks) {
	if (count($fallbacks) == 0) {
		return null;
	}
	if (count($fallbacks) == 1) {
		return $_ENV[$fallbacks[0]];
	}
	for ($i = 0; $i < count($fallbacks) - 1; $i++) {
		if (array_key_exists($fallbacks[$i], $_ENV)) {
			return $_ENV[$fallbacks[$i]];
		}
	}
	return $fallbacks[count($fallbacks) - 1];
}

$servers = array();
if (array_key_exists("POSTGRES_PASSWORD", $_ENV)) {
	$servers[env("POSTGRES_SERVER", "database")] = array(
		"username" => env("POSTGRES_USER", "postgres"),
		"pass" => env("POSTGRES_PASSWORD"),
		"label" => "PostgreSQL",
		"driver" => "pgsql",
		"databases" => array(env("POSTGRES_DB", "postgres") => env("POSTGRES_DB", "postgres"))
	);
}
if (array_key_exists("MYSQL_ROOT_PASSWORD", $_ENV) || array_key_exists("MYSQL_PASSWORD", $_ENV) || array_key_exists("MYSQL_ALLOW_EMPTY_PASSWORD", $_ENV)) {
	$servers[env("MYSQL_SERVER", "database")] = array(
		"username" => env("MYSQL_USER", "root"),
		"pass" => env("MYSQL_PASSWORD", "MYSQL_ROOT_PASSWORD", ""),
		"label" => "MySQL/MariaDB",
		"driver" => "server",
		"databases" => array(env("MYSQL_DATABASE", "mysql") => env("MYSQL_DATABASE", "mysql"))
	);
}
// TODO: add & test drivers for: clickhouse, elastic, firebird, mongo, mssql, oracle, simpledb, sqlite

if (count($servers) > 0) return new OneClickLogin($servers);
