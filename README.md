# Adminer OneClick

Adminer image for Docker with one click login support, based on [Adminer](https://www.adminer.org/), [the official Adminer Docker image](https://hub.docker.com/_/adminer) and [OneClick Login for Adminer](https://github.com/giofreitas/one-click-login).

It's built to use the same environment variables as the official database images, so it can be easily set up using an environment file, without unneccessary redundancy of the credentials.

## Usage

Create a Docker Compose file, either with an `env_file` containing your database credentials once for all containers, or manually like this:

```yaml
version: "3"
services:
  database: # If the database host isn't named "database", you must add the "MYSQL_SERVER" variable.
    image: mariadb
    environment:
      MYSQL_ROOT_PASSWORD: password
  adminer:
    image: momar/adminer-oneclick
    environment:
      MYSQL_ROOT_PASSWORD: password
```

**Supported drivers & environment variables:**

- MySQL
  - `MYSQL_SERVER` - server hostname
  - `MYSQL_USER` & `MYSQL_PASSWORD` - credentials
  - `MYSQL_ROOT_PASSWORD` - password for the root user if `MYSQL_USER` is unset
  - `MYSQL_ALLOW_EMPTY_PASSWORD` - needs to exist if neither `MYSQL_USER` nor `MYSQL_ROOT_PASSWORD` are specified to connect without a root password
  - `MYSQL_DATABASE` - name of the database, defaults to `mysql`
- PostgreSQL
  - `POSTGRES_SERVER` - server hostname
  - `POSTGRES_USER` & `POSTGRES_PASSWORD` - credentials (user defaults to `postgres`)
  - `POSTGRES_DB` - name of the database, defaults to `postgres`
